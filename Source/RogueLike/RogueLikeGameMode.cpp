// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "RogueLike.h"
#include "RogueLikeGameMode.h"
#include "RogueLikePlayerController.h"
#include "RogueLikeCharacter.h"

ARogueLikeGameMode::ARogueLikeGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ARogueLikePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}