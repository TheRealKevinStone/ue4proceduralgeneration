// Fill out your copyright notice in the Description page of Project Settings.

#include "RogueLike.h"
#include "DrawDebugHelpers.h"
#include "LevelGenerator.h"


// Sets default values
ALevelGenerator::ALevelGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ALevelGenerator::BeginPlay()
{
	Super::BeginPlay();

	// Check if we are going to use a random seed
	if (bUseRandomSeed)
		GenerationStream = FRandomStream(FDateTime::Now().ToUnixTimestamp());
	else
		GenerationStream = FRandomStream(Seed);

	// Initialize Pointers
	check(LevelSettingsClass)
	LevelSettings = NewObject<ULevelSettings>(this, LevelSettingsClass, TEXT("LevelSettings"));
	
	// Grab Info from our LevelSettings
	// Initialize our TileMap array to the proper size
	TileMap.SetNum(LevelSettings->Dimensions.X * LevelSettings->Dimensions.Y);
//	NeighbourTiles.SetNum(8);

	GenerateLevel();
}

// Called every frame
void ALevelGenerator::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ALevelGenerator::CarveLevel()
{

}

void ALevelGenerator::CheckNeighbouringTiles(int32 X, int32 Y)
{
	//NeighbourTiles[0] = X + LevelSettings->Dimensions.X * (Y + 1);		// North
	//NeighbourTiles[1] = X + 1 + LevelSettings->Dimensions.X * (Y + 1);	// North East
	//NeighbourTiles[2] = X + 1 + LevelSettings->Dimensions.X * Y;		// East
	//NeighbourTiles[3] = X + 1 + LevelSettings->Dimensions.X * (Y - 1);	// South East
	//NeighbourTiles[4] = X + LevelSettings->Dimensions.X * (Y - 1);		// South
	//NeighbourTiles[5] = X - 1 + LevelSettings->Dimensions.X * (Y - 1);	// South West
	//NeighbourTiles[6] = X - 1 + LevelSettings->Dimensions.X * Y;		// West
	//NeighbourTiles[7] = X - 1 + LevelSettings->Dimensions.X * (Y + 1);	// NorthWest
}

void ALevelGenerator::DrawDebug()
{
	for (int32 i = 0; i < TileMap.Num(); i++)
	{
		FVector Location = GetLocationFromIndex(i);
		FVector Dimensions = FVector(LevelSettings->TileSize * 0.5 - 5, LevelSettings->TileSize * 0.5 - 5, 10.f);
		FColor Color = TileMap[i] < 1 ? FColor(0, 255, 0) : FColor(0, 0, 255);

		DrawDebugBox(
			GetWorld(), 
			Location,
			Dimensions,
			Color,
			true
			);

	}
}

void ALevelGenerator::Evolve()
{
	for (int32 i = 0; i < TileMap.Num(); i++)
	{
		int X = i % (int32)LevelSettings->Dimensions.X;
		int Y = i / (int32)LevelSettings->Dimensions.Y;
		int32 Count = 0;
		
		if (X > 0 && X < LevelSettings->Dimensions.X - 1 && Y > 0 && Y < LevelSettings->Dimensions.Y - 1)
		{
			TArray<int32> NeighbourTiles;
			NeighbourTiles.SetNum(8);
			NeighbourTiles[0] = X + LevelSettings->Dimensions.X * (Y + 1);		// North
			NeighbourTiles[1] = X + 1 + LevelSettings->Dimensions.X * (Y + 1);	// North East
			NeighbourTiles[2] = X + 1 + LevelSettings->Dimensions.X * Y;		// East
			NeighbourTiles[3] = X + 1 + LevelSettings->Dimensions.X * (Y - 1);	// South East
			NeighbourTiles[4] = X + LevelSettings->Dimensions.X * (Y - 1);		// South
			NeighbourTiles[5] = X - 1 + LevelSettings->Dimensions.X * (Y - 1);	// South West
			NeighbourTiles[6] = X - 1 + LevelSettings->Dimensions.X * Y;		// West
			NeighbourTiles[7] = X - 1 + LevelSettings->Dimensions.X * (Y + 1);	// NorthWest

			for (int32 j = 0; j < NeighbourTiles.Num(); j++)
			{
				Count += TileMap[NeighbourTiles[j]];
			}
			if (Count > 4)
				TileMap[i] = 1;
			else if (Count < 4)
				TileMap[i] = 0;
		}
		else
			TileMap[i] = 1;
	}
}

void ALevelGenerator::GenerateLevel()
{
	RandomizeMap();
	for (int32 i = 0; i < EvolveIterations; i++)
	{
		Evolve();
	}
	DrawDebug();
//	PlaceTiles();
}

void ALevelGenerator::PlaceTiles()
{
	for (int32 i = 0; i < TileMap.Num(); i++)
	{
		FVector Location = GetLocationFromIndex(i);
		GetWorld()->SpawnActor<ALevelTile>(
			LevelTiles[TileMap[i]],
			Location,
			FRotator::ZeroRotator);
	}
}

void ALevelGenerator::RandomizeMap()
{
	for (int32 i = 0; i < TileMap.Num(); i++)
	{
		int32 rand = GenerationStream.FRandRange(1, 100) <= PercentChanceOfWall ? 1 : 0;
		TileMap[i] = rand;
	}

}

FVector ALevelGenerator::GetLocationFromIndex(int32 Index)
{
	int32 X = Index % (int32)LevelSettings->Dimensions.X;
	int32 Y = Index / (int32)LevelSettings->Dimensions.X;

	FVector Location = FVector(
		X * LevelSettings->TileSize,
		Y * LevelSettings->TileSize,
		0.f);

	return Location;
}
