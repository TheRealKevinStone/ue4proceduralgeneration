// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "LevelSettings.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class ROGUELIKE_API ULevelSettings : public UObject
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 TileSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector2D Dimensions;

public:
	ULevelSettings();
	
	
};
