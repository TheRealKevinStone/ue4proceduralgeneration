// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "LevelSettings.h"
#include "LevelTile.h"
#include "LevelGenerator.generated.h"

UCLASS()
class ROGUELIKE_API ALevelGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevelGenerator();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

public:
	UPROPERTY(EditAnywhere, BluePrintReadWrite)
	int32 Seed;

	UPROPERTY(EditAnywhere, BluePrintReadWrite)
	bool bUseRandomSeed;


protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 EvolveIterations;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 PercentChanceOfWall;

	UPROPERTY(EditAnywhere, BluePrintReadWrite)
	TSubclassOf<ULevelSettings> LevelSettingsClass;
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite)
	TArray < TSubclassOf<ALevelTile> > LevelTiles;

private:
	ULevelSettings* LevelSettings;
	FRandomStream GenerationStream;
	TArray<int8> TileMap;

	FDateTime SpawnTime;

private:
	void CarveLevel();
	void CheckNeighbouringTiles(int32 X, int32 Y);
	void DrawDebug();
	void Evolve();
	void GenerateLevel();
	void PlaceTiles();
	void RandomizeMap();

	FVector GetLocationFromIndex(int32 Index);
	
};
