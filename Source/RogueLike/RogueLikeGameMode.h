// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "RogueLikeGameMode.generated.h"

UCLASS(minimalapi)
class ARogueLikeGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ARogueLikeGameMode();
};



